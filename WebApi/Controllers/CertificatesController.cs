﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.IO;
using Org.BouncyCastle.Asn1.X509;
using System.Text;
using System.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/certificates")]
    [ApiController]
    public class CertificatesController : ControllerBase
    {
        // GET: api/<CertificatesController>
        [HttpGet("adoptioncertificate")]
        public async Task<string> GetAdoptionCertificateAsync(int adoptionId)
        {
            var content = await System.IO.File.ReadAllTextAsync("Documents/adoption.html");

            content = content.Replace("@DateOfAdoption", DateTime.Now.ToString());
            content = content.Replace("@ChildName","Child");
            content = content.Replace("@FatherName","Father");
            content = content.Replace("@MotherName", "Mother");
         
            return content;
        }

        // GET: api/<CertificatesController>
        [HttpGet("marriagecertificate")]
        public async Task<string> GetMarriageCertificateAsync(int marriageId)
        {
            var content = await System.IO.File.ReadAllTextAsync("Documents/marriage.html");

            return content;
        }

        // GET: api/<CertificatesController>
        [HttpGet("divorcecertificate")]
        public async Task<string> GetDivorceCertificateAsync(int marriageId)
        {
            var content = await System.IO.File.ReadAllTextAsync("Documents/divorce.html");

            return content;
        }

        // GET: api/<CertificatesController>
        [HttpGet("deathcertificate")]
        public async Task<string> GetDeathCertificateAsync(int personId)
        {
            var content = await System.IO.File.ReadAllTextAsync("Documents/death.html");

            return content;
        }
    }
}
