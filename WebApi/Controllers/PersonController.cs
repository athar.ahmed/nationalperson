﻿using Dtos.Request;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/persons")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        // GET: api/<PersonController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            return Ok();
        }

        // GET api/<PersonController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            return Ok();
        }

        // POST api/<PersonController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromForm] PersonWrapper wrapper)
        {
            return Ok();
        }

        // PUT api/<PersonController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int personId, [FromBody] PersonWrapper wrapper)
        {
            return Ok();
        }

        // DELETE api/<PersonController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int personId)
        {
            return Ok();
        }
    }
}
