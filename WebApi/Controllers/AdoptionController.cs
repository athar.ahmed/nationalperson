﻿using Dtos.Request;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/adoptions")]
    [ApiController]
    public class AdoptionController : ControllerBase
    {
        // GET: api/<AdoptionController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            // get all data from adoption table
            return Ok();
        }

        // GET api/<AdoptionController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int adoptionId)
        {
            return Ok();
        }

        // GET api/<AdoptionController>/5
        [HttpGet("{adopterid}")]
        public async Task<IActionResult> GetDataUsingAdopterIDAsync(int adopterId)
        {
            return Ok();
        }

        // GET api/<AdoptionController>/5
        [HttpGet("{childid}")]
        public async Task<IActionResult> GetDataUsingChildIDAsync(int childId)
        {
            return Ok();
        }

        // POST api/<AdoptionController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] AdoptionWrapper wrapper)
        {
            return Ok();
        }

        // PUT api/<AdoptionController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int adoptionId, [FromBody] AdoptionWrapper wrapper)
        {
            return Ok();
        }

        // DELETE api/<AdoptionController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int adoptionId)
        {
            return Ok();
        }
    }
}
