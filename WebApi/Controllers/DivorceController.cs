﻿using Dtos.Request;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/divorceseparations")]
    [ApiController]
    public class DivorceController : ControllerBase
    {
        // GET: api/<DivorceController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            // get all data from marriage where dateofdivorce is not null
            return Ok();
        }

        // POST api/<DivorceController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] DivorceWrapper divorceWrapper)
        {
            return Ok();
        }

        // PUT api/<DivorceController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int marriageId, [FromBody] DivorceWrapper divorceWrapper)
        {
            return Ok();
        }
    }
}
