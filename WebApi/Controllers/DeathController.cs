﻿using Dtos.Request;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/deaths")]
    [ApiController]
    public class DeathController : ControllerBase
    {
        // GET: api/<DeathController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            //get all data where DateOfDeath is not null
            return Ok();
        }

        // GET api/<DeathController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int personId)
        {
            //get data where DateOfDeath is not null and personId is input
            return Ok();
        }

        // POST api/<DeathController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] DeathWrapper wrapper)
        {
            //update person table with dateofdeath 
            return Ok();
        }

        // PUT api/<DeathController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int personId, [FromBody] DeathWrapper wrapper)
        {
            return Ok();
        }
    }
}
