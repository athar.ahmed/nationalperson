﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/reports")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        // GET: api/<ReportsController>
        [HttpGet("{personId:int}/siblings")]
        public List<string> GetSiblings(int personid)
        {
            List<string> tempList = new List<string>();
            tempList.Add("Name");
            tempList.Add("DOB");
            tempList.Add("Address");
            return tempList;
        }

        [HttpGet("{personId:int}/parents")]
        public List<string> GetParents(int personid)
        {
            List<string> tempList = new List<string>();
            tempList.Add("Name");
            tempList.Add("DOB");
            tempList.Add("Address");
            return tempList;
        }
    }
}
