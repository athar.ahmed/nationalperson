﻿using Dtos.Request;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/familyunits")]
    [ApiController]
    public class FamilyUnitController : ControllerBase
    {
        // GET: api/<FamilyUnitController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            return Ok();
        }

        // GET api/<FamilyUnitController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int familyUnitId)
        {
            return Ok(); 
        }

        // POST api/<FamilyUnitController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] FamilyUnitWrapper wrapper)
        {
            return Ok();
        }

        // PUT api/<FamilyUnitController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int familyUnitId, [FromBody] FamilyUnitWrapper wrapper)
        {
            return Ok();
        }

        // DELETE api/<FamilyUnitController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int familyUnitId)
        {
            return Ok();
        }
    }
}
