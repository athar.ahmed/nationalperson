﻿using Dtos.Request;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/marriages")]
    [ApiController]
    public class MarriageController : ControllerBase
    {
        // GET: api/<MarriageController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            return Ok();
        }

        // GET api/<MarriageController>/5
        [HttpGet("{husbandid}")]
        public async Task<IActionResult> GetDataUsingHusbandIDAsync(int husbandId)
        {
            // get data using husbandid
            return Ok();
        }

        // GET api/<MarriageController>/5
        [HttpGet("{wifeid}")]
        public async Task<IActionResult> GetDataUsingWifeIDAsync(int wifeId)
        {
            // get data using wifeid
            return Ok();
        }

        // POST api/<MarriageController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] MarriageWrapper wrapper)
        {
            return Ok();
        }

        // PUT api/<MarriageController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int marriageId, [FromBody] MarriageWrapper wrapper)
        {
            return Ok();
        }

        // DELETE api/<MarriageController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int marriageId)
        {
            return Ok();
        }
    }
}
