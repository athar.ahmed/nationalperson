﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Identity.Client;
using Dtos.Enum;

namespace Entities
{
    public class Person
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string FirstName { get; set; } = string.Empty;

        public string MiddleName { get; set; }  = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Cnic { get; set; } = string.Empty;

        public DateTime DateOfBirth { get; set; }

        public DateTime? DateOfDeath { get; set; }

        public string ReasonOfDeath { get; set; } = string.Empty;

        public int? FatherID { get; set; }

        public int? MotherID { get; set; }

        public Place PlaceOfBirth { get; set; }

        public Gender Gender{ get; set; }

        public int FamilyUnitID { get; set; }

        public FamilyUnit FamilyUnit { get; set; }

        public Person Father { get; set; }

        public Person Mother { get; set; } 

        public List<Person> Persons { get; set; } = new();

        public List<Marriage> Marriages { get; set; } = new();

        public List<Adoption> Adoptions { get; set; } = new();
    }
}
