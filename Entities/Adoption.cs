﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Adoption
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int AdopterID { get; set; }

        public int ChildID { get; set; }

        public DateTime DateOfAdoption { get; set; }

        public Person Adopter { get; set; }

        public Person Child { get; set; }
    }
}
