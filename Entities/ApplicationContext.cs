﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Adoption> Adoptions { get; set; }

        public DbSet<Marriage> Marriages { get; set; }

        public DbSet<FamilyUnit> FamilyUnits { get; set; }

        public DbSet<Person> Persons { get; set; }
        
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }
    }
}
