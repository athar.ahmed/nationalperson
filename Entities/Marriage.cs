﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Marriage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int HusbandID { get; set; }

        public int WifeID { get; set; } 

        public DateTime DateOfMarriage { get; set; }

        public DateTime? DateOfDivorce { get; set; }

        public DateTime? DateOfSeparation { get; set; }

        public string ReasonOfSeparation { get; set; } = string.Empty;

        public Person Husband { get; set; }

        public Person Wife { get; set; }
    }
}
