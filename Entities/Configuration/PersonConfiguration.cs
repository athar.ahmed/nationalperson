﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Entities.Configuration
{
    class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasOne(x => x.Father).WithMany(x => x.Persons).HasForeignKey(x => x.FatherID);

            builder.HasOne(x => x.Mother).WithMany(x => x.Persons).HasForeignKey(x => x.MotherID);

            builder
            .HasOne(da => da.FamilyUnit)
            .WithMany(a => a.Persons)
            .HasForeignKey(da => da.FamilyUnitID);
        }
    }
}
