﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configuration
{
    class AdoptionConfiguration : IEntityTypeConfiguration<Adoption>
    {
        public void Configure(EntityTypeBuilder<Adoption> builder)
        {
            builder
            .HasOne(da => da.Adopter)
            .WithMany(a => a.Adoptions)
            .HasForeignKey(da => da.AdopterID);

            builder
            .HasOne(da => da.Child)
            .WithMany(a => a.Adoptions)
            .HasForeignKey(da => da.ChildID);
        }
    }
}
