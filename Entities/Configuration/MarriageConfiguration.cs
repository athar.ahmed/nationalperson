﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configuration
{
    class MarriageConfiguration : IEntityTypeConfiguration<Marriage>
    {
        public void Configure(EntityTypeBuilder<Marriage> builder)
        {
            builder
            .HasOne(da => da.Husband)
            .WithMany(a => a.Marriages)
            .HasForeignKey(da => da.HusbandID);

            builder
            .HasOne(da => da.Wife)
            .WithMany(a => a.Marriages)
            .HasForeignKey(da => da.WifeID);
        }
    }
}
