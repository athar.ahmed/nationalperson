﻿namespace WebServiceForVendor
{
    public class SecureToken
    {
        public static string GenerateSecureToken(string userName)
        {
            byte[] time = BitConverter.GetBytes(DateTime.Now.Ticks);
            byte[] key = new Guid().ToByteArray();
            byte[] user = GetBytes(userName.PadRight(500 - userName.Length));
            string token = Convert.ToBase64String(time.Concat(user.Concat(key)).ToArray());

            return token;
        }

        public static bool ValidateToken(string token, string userName)
        {
            bool validToken = true;
            bool IsSessionTimedOut = false;
            bool IsEmailNotMacthing = false;
            token.Replace(@"\", "");
            byte[] data = Convert.FromBase64String(token);
            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
            byte[] user = GetBytes(userName.PadRight(500 - userName.Length));
            byte[] subData = new byte[user.Length];
            Array.Copy(data, BitConverter.GetBytes(when.Ticks).Length, subData, 0, user.Length);
            string usr = GetString(subData);

            if (!usr.Trim().Equals(userName))
            {
                IsEmailNotMacthing = true;
            }

            if (when < DateTime.Now.AddHours(-24))
            {
                IsSessionTimedOut = true;
            }

            if (IsEmailNotMacthing == true || IsSessionTimedOut == true)
                validToken = false;

            return validToken;
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
