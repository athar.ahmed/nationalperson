﻿using System.ComponentModel.DataAnnotations;

namespace WebServiceForVendor.VendorDtos
{
    public record TokenRequest([Required] string UserName, [Required] string Password);
}
