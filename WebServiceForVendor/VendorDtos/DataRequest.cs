﻿using System.Runtime.Serialization;

namespace WebServiceForVendor.VendorDtos
{
    public class DataRequest
    {
        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Cnic { get; set; }
    }
}
