﻿using Microsoft.AspNetCore.Mvc;
using WebServiceForVendor.VendorDtos;

namespace WebServiceForVendor.Controllers
{
    [Route("api/nationalPersonWebService")]
    [ApiController]
    public class NationalVendorController : ControllerBase
    {
        // GET: api/<NationalVendorController>
        [HttpGet("token")]
        public async Task<IActionResult> GetTokenAsync(TokenRequest tokenRequest)
        {
            string userName = tokenRequest.UserName;
            string password = tokenRequest.Password;

            if (!AuthorizeUser(userName, password))
            {
                return Forbid();
            }
            string token = SecureToken.GenerateSecureToken(userName);

            return Ok(token);
        }

        // GET: api/<NationalVendorController>
        [HttpGet("person/cnic")]
        public async Task<IActionResult> GetPersonDataUsingCnicAsync(DataRequest dataRequest)
        {
            if (!SecureToken.ValidateToken(dataRequest.Token, dataRequest.Username))
            {
                return Forbid();
            }

            //get data of person and familyunit using cnic
            return Ok();
        }

        public bool AuthorizeUser(string user, string password)
        {
            return true;
        }
    }
}
