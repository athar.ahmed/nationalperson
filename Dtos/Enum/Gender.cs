﻿namespace Dtos.Enum
{
    public enum Gender
    {
        Male,
        Female,
        DoNotDisclose,
    }
}