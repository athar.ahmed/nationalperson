﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dtos.Request
{
    public record DeathWrapper([Required] int PersonID, DateTime DateOfDeath, string Reason);

}
