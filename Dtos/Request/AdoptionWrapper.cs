﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dtos.Request
{
    public record AdoptionWrapper([Required] int AdopterID, [Required] int ChildID, [Required] DateTime DateOfAdoption);
}
