﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dtos.Request
{
    public record PersonWrapper([Required] string FirstName, string MiddleName, string LastName, [Required] string Cnic, [Required] DateTime DateOfBirth, int PlaceOfBirth, [Required] int Gender, [Required] int FamilyUnit, int FatherID, int MotherID );
}
