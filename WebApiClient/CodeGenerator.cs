﻿namespace WebApiClient
{
    public class CodeGenerator
    {


        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class AdoptionClient
        {
            private string _baseUrl = "https://localhost:7295";
            private System.Net.Http.HttpClient _httpClient;
            private System.Lazy<Newtonsoft.Json.JsonSerializerSettings> _settings;

            public AdoptionClient(System.Net.Http.HttpClient httpClient)
            {
                _httpClient = httpClient;
                _settings = new System.Lazy<Newtonsoft.Json.JsonSerializerSettings>(CreateSerializerSettings);
            }

            private Newtonsoft.Json.JsonSerializerSettings CreateSerializerSettings()
            {
                var settings = new Newtonsoft.Json.JsonSerializerSettings();
                UpdateJsonSerializerSettings(settings);
                return settings;
            }

            public string BaseUrl
            {
                get { return _baseUrl; }
                set { _baseUrl = value; }
            }

            protected Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

            partial void UpdateJsonSerializerSettings(Newtonsoft.Json.JsonSerializerSettings settings);

            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, string url);
            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, System.Text.StringBuilder urlBuilder);
            partial void ProcessResponse(System.Net.Http.HttpClient client, System.Net.Http.HttpResponseMessage response);

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetAsync()
            {
                return GetAsync(System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetAsync(System.Threading.CancellationToken cancellationToken)
            {
                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/adoptions");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PostAsync(AdoptionWrapper wrapper)
            {
                return PostAsync(wrapper, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PostAsync(AdoptionWrapper wrapper, System.Threading.CancellationToken cancellationToken)
            {
                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/adoptions");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("POST");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> Get2Async(int? adoptionId, string id)
            {
                return Get2Async(adoptionId, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> Get2Async(int? adoptionId, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/adoptions/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (adoptionId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("adoptionId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(adoptionId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PutAsync(int? adoptionId, AdoptionWrapper wrapper, string id)
            {
                return PutAsync(adoptionId, wrapper, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PutAsync(int? adoptionId, AdoptionWrapper wrapper, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/adoptions/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (adoptionId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("adoptionId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(adoptionId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("PUT");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> DeleteAsync(int? adoptionId, string id)
            {
                return DeleteAsync(adoptionId, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> DeleteAsync(int? adoptionId, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/adoptions/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (adoptionId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("adoptionId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(adoptionId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("DELETE");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetDataUsingAdopterIDAsync(int adopterId)
            {
                return GetDataUsingAdopterIDAsync(adopterId, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetDataUsingAdopterIDAsync(int adopterId, System.Threading.CancellationToken cancellationToken)
            {
                if (adopterId == null)
                    throw new System.ArgumentNullException("adopterId");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/adoptions/{adopterid}");
                urlBuilder_.Replace("{adopterId}", System.Uri.EscapeDataString(ConvertToString(adopterId, System.Globalization.CultureInfo.InvariantCulture)));

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetDataUsingChildIDAsync(int childId)
            {
                return GetDataUsingChildIDAsync(childId, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetDataUsingChildIDAsync(int childId, System.Threading.CancellationToken cancellationToken)
            {
                if (childId == null)
                    throw new System.ArgumentNullException("childId");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/adoptions/{childid}");
                urlBuilder_.Replace("{childId}", System.Uri.EscapeDataString(ConvertToString(childId, System.Globalization.CultureInfo.InvariantCulture)));

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            protected struct ObjectResponseResult<T>
            {
                public ObjectResponseResult(T responseObject, string responseText)
                {
                    this.Object = responseObject;
                    this.Text = responseText;
                }

                public T Object { get; }

                public string Text { get; }
            }

            public bool ReadResponseAsString { get; set; }

            protected virtual async System.Threading.Tasks.Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(System.Net.Http.HttpResponseMessage response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.Threading.CancellationToken cancellationToken)
            {
                if (response == null || response.Content == null)
                {
                    return new ObjectResponseResult<T>(default(T), string.Empty);
                }

                if (ReadResponseAsString)
                {
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    try
                    {
                        var typedBody = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
                        return new ObjectResponseResult<T>(typedBody, responseText);
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
                    }
                }
                else
                {
                    try
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        using (var streamReader = new System.IO.StreamReader(responseStream))
                        using (var jsonTextReader = new Newtonsoft.Json.JsonTextReader(streamReader))
                        {
                            var serializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings);
                            var typedBody = serializer.Deserialize<T>(jsonTextReader);
                            return new ObjectResponseResult<T>(typedBody, string.Empty);
                        }
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
                    }
                }
            }

            private string ConvertToString(object value, System.Globalization.CultureInfo cultureInfo)
            {
                if (value == null)
                {
                    return "";
                }

                if (value is System.Enum)
                {
                    var name = System.Enum.GetName(value.GetType(), value);
                    if (name != null)
                    {
                        var field = System.Reflection.IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
                        if (field != null)
                        {
                            var attribute = System.Reflection.CustomAttributeExtensions.GetCustomAttribute(field, typeof(System.Runtime.Serialization.EnumMemberAttribute))
                                as System.Runtime.Serialization.EnumMemberAttribute;
                            if (attribute != null)
                            {
                                return attribute.Value != null ? attribute.Value : name;
                            }
                        }

                        var converted = System.Convert.ToString(System.Convert.ChangeType(value, System.Enum.GetUnderlyingType(value.GetType()), cultureInfo));
                        return converted == null ? string.Empty : converted;
                    }
                }
                else if (value is bool)
                {
                    return System.Convert.ToString((bool)value, cultureInfo).ToLowerInvariant();
                }
                else if (value is byte[])
                {
                    return System.Convert.ToBase64String((byte[])value);
                }
                else if (value.GetType().IsArray)
                {
                    var array = System.Linq.Enumerable.OfType<object>((System.Array)value);
                    return string.Join(",", System.Linq.Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
                }

                var result = System.Convert.ToString(value, cultureInfo);
                return result == null ? "" : result;
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class DeathClient
        {
            private string _baseUrl = "https://localhost:7295";
            private System.Net.Http.HttpClient _httpClient;
            private System.Lazy<Newtonsoft.Json.JsonSerializerSettings> _settings;

            public DeathClient(System.Net.Http.HttpClient httpClient)
            {
                _httpClient = httpClient;
                _settings = new System.Lazy<Newtonsoft.Json.JsonSerializerSettings>(CreateSerializerSettings);
            }

            private Newtonsoft.Json.JsonSerializerSettings CreateSerializerSettings()
            {
                var settings = new Newtonsoft.Json.JsonSerializerSettings();
                UpdateJsonSerializerSettings(settings);
                return settings;
            }

            public string BaseUrl
            {
                get { return _baseUrl; }
                set { _baseUrl = value; }
            }

            protected Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

            partial void UpdateJsonSerializerSettings(Newtonsoft.Json.JsonSerializerSettings settings);

            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, string url);
            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, System.Text.StringBuilder urlBuilder);
            partial void ProcessResponse(System.Net.Http.HttpClient client, System.Net.Http.HttpResponseMessage response);

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetAsync()
            {
                return GetAsync(System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetAsync(System.Threading.CancellationToken cancellationToken)
            {
                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/deaths");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PostAsync(DeathWrapper wrapper)
            {
                return PostAsync(wrapper, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PostAsync(DeathWrapper wrapper, System.Threading.CancellationToken cancellationToken)
            {
                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/deaths");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("POST");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> Get2Async(int? personId, string id)
            {
                return Get2Async(personId, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> Get2Async(int? personId, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/deaths/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (personId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("personId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(personId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PutAsync(int? personId, DeathWrapper wrapper, string id)
            {
                return PutAsync(personId, wrapper, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PutAsync(int? personId, DeathWrapper wrapper, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/deaths/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (personId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("personId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(personId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("PUT");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            protected struct ObjectResponseResult<T>
            {
                public ObjectResponseResult(T responseObject, string responseText)
                {
                    this.Object = responseObject;
                    this.Text = responseText;
                }

                public T Object { get; }

                public string Text { get; }
            }

            public bool ReadResponseAsString { get; set; }

            protected virtual async System.Threading.Tasks.Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(System.Net.Http.HttpResponseMessage response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.Threading.CancellationToken cancellationToken)
            {
                if (response == null || response.Content == null)
                {
                    return new ObjectResponseResult<T>(default(T), string.Empty);
                }

                if (ReadResponseAsString)
                {
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    try
                    {
                        var typedBody = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
                        return new ObjectResponseResult<T>(typedBody, responseText);
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
                    }
                }
                else
                {
                    try
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        using (var streamReader = new System.IO.StreamReader(responseStream))
                        using (var jsonTextReader = new Newtonsoft.Json.JsonTextReader(streamReader))
                        {
                            var serializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings);
                            var typedBody = serializer.Deserialize<T>(jsonTextReader);
                            return new ObjectResponseResult<T>(typedBody, string.Empty);
                        }
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
                    }
                }
            }

            private string ConvertToString(object value, System.Globalization.CultureInfo cultureInfo)
            {
                if (value == null)
                {
                    return "";
                }

                if (value is System.Enum)
                {
                    var name = System.Enum.GetName(value.GetType(), value);
                    if (name != null)
                    {
                        var field = System.Reflection.IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
                        if (field != null)
                        {
                            var attribute = System.Reflection.CustomAttributeExtensions.GetCustomAttribute(field, typeof(System.Runtime.Serialization.EnumMemberAttribute))
                                as System.Runtime.Serialization.EnumMemberAttribute;
                            if (attribute != null)
                            {
                                return attribute.Value != null ? attribute.Value : name;
                            }
                        }

                        var converted = System.Convert.ToString(System.Convert.ChangeType(value, System.Enum.GetUnderlyingType(value.GetType()), cultureInfo));
                        return converted == null ? string.Empty : converted;
                    }
                }
                else if (value is bool)
                {
                    return System.Convert.ToString((bool)value, cultureInfo).ToLowerInvariant();
                }
                else if (value is byte[])
                {
                    return System.Convert.ToBase64String((byte[])value);
                }
                else if (value.GetType().IsArray)
                {
                    var array = System.Linq.Enumerable.OfType<object>((System.Array)value);
                    return string.Join(",", System.Linq.Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
                }

                var result = System.Convert.ToString(value, cultureInfo);
                return result == null ? "" : result;
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class DivorceClient
        {
            private string _baseUrl = "https://localhost:7295";
            private System.Net.Http.HttpClient _httpClient;
            private System.Lazy<Newtonsoft.Json.JsonSerializerSettings> _settings;

            public DivorceClient(System.Net.Http.HttpClient httpClient)
            {
                _httpClient = httpClient;
                _settings = new System.Lazy<Newtonsoft.Json.JsonSerializerSettings>(CreateSerializerSettings);
            }

            private Newtonsoft.Json.JsonSerializerSettings CreateSerializerSettings()
            {
                var settings = new Newtonsoft.Json.JsonSerializerSettings();
                UpdateJsonSerializerSettings(settings);
                return settings;
            }

            public string BaseUrl
            {
                get { return _baseUrl; }
                set { _baseUrl = value; }
            }

            protected Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

            partial void UpdateJsonSerializerSettings(Newtonsoft.Json.JsonSerializerSettings settings);

            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, string url);
            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, System.Text.StringBuilder urlBuilder);
            partial void ProcessResponse(System.Net.Http.HttpClient client, System.Net.Http.HttpResponseMessage response);

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetAsync()
            {
                return GetAsync(System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetAsync(System.Threading.CancellationToken cancellationToken)
            {
                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/divorceseparations");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PostAsync(DivorceWrapper divorceWrapper)
            {
                return PostAsync(divorceWrapper, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PostAsync(DivorceWrapper divorceWrapper, System.Threading.CancellationToken cancellationToken)
            {
                if (divorceWrapper == null)
                    throw new System.ArgumentNullException("divorceWrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/divorceseparations");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(divorceWrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("POST");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PutAsync(int? marriageId, DivorceWrapper divorceWrapper, string id)
            {
                return PutAsync(marriageId, divorceWrapper, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PutAsync(int? marriageId, DivorceWrapper divorceWrapper, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                if (divorceWrapper == null)
                    throw new System.ArgumentNullException("divorceWrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/divorceseparations/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (marriageId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("marriageId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(marriageId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(divorceWrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("PUT");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            protected struct ObjectResponseResult<T>
            {
                public ObjectResponseResult(T responseObject, string responseText)
                {
                    this.Object = responseObject;
                    this.Text = responseText;
                }

                public T Object { get; }

                public string Text { get; }
            }

            public bool ReadResponseAsString { get; set; }

            protected virtual async System.Threading.Tasks.Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(System.Net.Http.HttpResponseMessage response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.Threading.CancellationToken cancellationToken)
            {
                if (response == null || response.Content == null)
                {
                    return new ObjectResponseResult<T>(default(T), string.Empty);
                }

                if (ReadResponseAsString)
                {
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    try
                    {
                        var typedBody = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
                        return new ObjectResponseResult<T>(typedBody, responseText);
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
                    }
                }
                else
                {
                    try
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        using (var streamReader = new System.IO.StreamReader(responseStream))
                        using (var jsonTextReader = new Newtonsoft.Json.JsonTextReader(streamReader))
                        {
                            var serializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings);
                            var typedBody = serializer.Deserialize<T>(jsonTextReader);
                            return new ObjectResponseResult<T>(typedBody, string.Empty);
                        }
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
                    }
                }
            }

            private string ConvertToString(object value, System.Globalization.CultureInfo cultureInfo)
            {
                if (value == null)
                {
                    return "";
                }

                if (value is System.Enum)
                {
                    var name = System.Enum.GetName(value.GetType(), value);
                    if (name != null)
                    {
                        var field = System.Reflection.IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
                        if (field != null)
                        {
                            var attribute = System.Reflection.CustomAttributeExtensions.GetCustomAttribute(field, typeof(System.Runtime.Serialization.EnumMemberAttribute))
                                as System.Runtime.Serialization.EnumMemberAttribute;
                            if (attribute != null)
                            {
                                return attribute.Value != null ? attribute.Value : name;
                            }
                        }

                        var converted = System.Convert.ToString(System.Convert.ChangeType(value, System.Enum.GetUnderlyingType(value.GetType()), cultureInfo));
                        return converted == null ? string.Empty : converted;
                    }
                }
                else if (value is bool)
                {
                    return System.Convert.ToString((bool)value, cultureInfo).ToLowerInvariant();
                }
                else if (value is byte[])
                {
                    return System.Convert.ToBase64String((byte[])value);
                }
                else if (value.GetType().IsArray)
                {
                    var array = System.Linq.Enumerable.OfType<object>((System.Array)value);
                    return string.Join(",", System.Linq.Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
                }

                var result = System.Convert.ToString(value, cultureInfo);
                return result == null ? "" : result;
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class FamilyUnitClient
        {
            private string _baseUrl = "https://localhost:7295";
            private System.Net.Http.HttpClient _httpClient;
            private System.Lazy<Newtonsoft.Json.JsonSerializerSettings> _settings;

            public FamilyUnitClient(System.Net.Http.HttpClient httpClient)
            {
                _httpClient = httpClient;
                _settings = new System.Lazy<Newtonsoft.Json.JsonSerializerSettings>(CreateSerializerSettings);
            }

            private Newtonsoft.Json.JsonSerializerSettings CreateSerializerSettings()
            {
                var settings = new Newtonsoft.Json.JsonSerializerSettings();
                UpdateJsonSerializerSettings(settings);
                return settings;
            }

            public string BaseUrl
            {
                get { return _baseUrl; }
                set { _baseUrl = value; }
            }

            protected Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

            partial void UpdateJsonSerializerSettings(Newtonsoft.Json.JsonSerializerSettings settings);

            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, string url);
            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, System.Text.StringBuilder urlBuilder);
            partial void ProcessResponse(System.Net.Http.HttpClient client, System.Net.Http.HttpResponseMessage response);

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetAsync()
            {
                return GetAsync(System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetAsync(System.Threading.CancellationToken cancellationToken)
            {
                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/familyunits");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PostAsync(FamilyUnitWrapper wrapper)
            {
                return PostAsync(wrapper, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PostAsync(FamilyUnitWrapper wrapper, System.Threading.CancellationToken cancellationToken)
            {
                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/familyunits");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("POST");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> Get2Async(int? familyUnitId, string id)
            {
                return Get2Async(familyUnitId, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> Get2Async(int? familyUnitId, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/familyunits/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (familyUnitId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("familyUnitId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(familyUnitId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PutAsync(int? familyUnitId, FamilyUnitWrapper wrapper, string id)
            {
                return PutAsync(familyUnitId, wrapper, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PutAsync(int? familyUnitId, FamilyUnitWrapper wrapper, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/familyunits/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (familyUnitId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("familyUnitId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(familyUnitId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("PUT");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> DeleteAsync(int? familyUnitId, string id)
            {
                return DeleteAsync(familyUnitId, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> DeleteAsync(int? familyUnitId, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/familyunits/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (familyUnitId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("familyUnitId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(familyUnitId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("DELETE");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            protected struct ObjectResponseResult<T>
            {
                public ObjectResponseResult(T responseObject, string responseText)
                {
                    this.Object = responseObject;
                    this.Text = responseText;
                }

                public T Object { get; }

                public string Text { get; }
            }

            public bool ReadResponseAsString { get; set; }

            protected virtual async System.Threading.Tasks.Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(System.Net.Http.HttpResponseMessage response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.Threading.CancellationToken cancellationToken)
            {
                if (response == null || response.Content == null)
                {
                    return new ObjectResponseResult<T>(default(T), string.Empty);
                }

                if (ReadResponseAsString)
                {
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    try
                    {
                        var typedBody = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
                        return new ObjectResponseResult<T>(typedBody, responseText);
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
                    }
                }
                else
                {
                    try
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        using (var streamReader = new System.IO.StreamReader(responseStream))
                        using (var jsonTextReader = new Newtonsoft.Json.JsonTextReader(streamReader))
                        {
                            var serializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings);
                            var typedBody = serializer.Deserialize<T>(jsonTextReader);
                            return new ObjectResponseResult<T>(typedBody, string.Empty);
                        }
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
                    }
                }
            }

            private string ConvertToString(object value, System.Globalization.CultureInfo cultureInfo)
            {
                if (value == null)
                {
                    return "";
                }

                if (value is System.Enum)
                {
                    var name = System.Enum.GetName(value.GetType(), value);
                    if (name != null)
                    {
                        var field = System.Reflection.IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
                        if (field != null)
                        {
                            var attribute = System.Reflection.CustomAttributeExtensions.GetCustomAttribute(field, typeof(System.Runtime.Serialization.EnumMemberAttribute))
                                as System.Runtime.Serialization.EnumMemberAttribute;
                            if (attribute != null)
                            {
                                return attribute.Value != null ? attribute.Value : name;
                            }
                        }

                        var converted = System.Convert.ToString(System.Convert.ChangeType(value, System.Enum.GetUnderlyingType(value.GetType()), cultureInfo));
                        return converted == null ? string.Empty : converted;
                    }
                }
                else if (value is bool)
                {
                    return System.Convert.ToString((bool)value, cultureInfo).ToLowerInvariant();
                }
                else if (value is byte[])
                {
                    return System.Convert.ToBase64String((byte[])value);
                }
                else if (value.GetType().IsArray)
                {
                    var array = System.Linq.Enumerable.OfType<object>((System.Array)value);
                    return string.Join(",", System.Linq.Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
                }

                var result = System.Convert.ToString(value, cultureInfo);
                return result == null ? "" : result;
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class MarriageClient
        {
            private string _baseUrl = "https://localhost:7295";
            private System.Net.Http.HttpClient _httpClient;
            private System.Lazy<Newtonsoft.Json.JsonSerializerSettings> _settings;

            public MarriageClient(System.Net.Http.HttpClient httpClient)
            {
                _httpClient = httpClient;
                _settings = new System.Lazy<Newtonsoft.Json.JsonSerializerSettings>(CreateSerializerSettings);
            }

            private Newtonsoft.Json.JsonSerializerSettings CreateSerializerSettings()
            {
                var settings = new Newtonsoft.Json.JsonSerializerSettings();
                UpdateJsonSerializerSettings(settings);
                return settings;
            }

            public string BaseUrl
            {
                get { return _baseUrl; }
                set { _baseUrl = value; }
            }

            protected Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

            partial void UpdateJsonSerializerSettings(Newtonsoft.Json.JsonSerializerSettings settings);

            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, string url);
            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, System.Text.StringBuilder urlBuilder);
            partial void ProcessResponse(System.Net.Http.HttpClient client, System.Net.Http.HttpResponseMessage response);

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetAsync()
            {
                return GetAsync(System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetAsync(System.Threading.CancellationToken cancellationToken)
            {
                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/marriages");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PostAsync(MarriageWrapper wrapper)
            {
                return PostAsync(wrapper, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PostAsync(MarriageWrapper wrapper, System.Threading.CancellationToken cancellationToken)
            {
                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/marriages");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("POST");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetDataUsingHusbandIDAsync(int husbandId)
            {
                return GetDataUsingHusbandIDAsync(husbandId, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetDataUsingHusbandIDAsync(int husbandId, System.Threading.CancellationToken cancellationToken)
            {
                if (husbandId == null)
                    throw new System.ArgumentNullException("husbandId");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/marriages/{husbandid}");
                urlBuilder_.Replace("{husbandId}", System.Uri.EscapeDataString(ConvertToString(husbandId, System.Globalization.CultureInfo.InvariantCulture)));

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetDataUsingWifeIDAsync(int wifeId)
            {
                return GetDataUsingWifeIDAsync(wifeId, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetDataUsingWifeIDAsync(int wifeId, System.Threading.CancellationToken cancellationToken)
            {
                if (wifeId == null)
                    throw new System.ArgumentNullException("wifeId");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/marriages/{wifeid}");
                urlBuilder_.Replace("{wifeId}", System.Uri.EscapeDataString(ConvertToString(wifeId, System.Globalization.CultureInfo.InvariantCulture)));

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PutAsync(int? marriageId, MarriageWrapper wrapper, string id)
            {
                return PutAsync(marriageId, wrapper, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PutAsync(int? marriageId, MarriageWrapper wrapper, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/marriages/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (marriageId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("marriageId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(marriageId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("PUT");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> DeleteAsync(int? marriageId, string id)
            {
                return DeleteAsync(marriageId, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> DeleteAsync(int? marriageId, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/marriages/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (marriageId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("marriageId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(marriageId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("DELETE");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            protected struct ObjectResponseResult<T>
            {
                public ObjectResponseResult(T responseObject, string responseText)
                {
                    this.Object = responseObject;
                    this.Text = responseText;
                }

                public T Object { get; }

                public string Text { get; }
            }

            public bool ReadResponseAsString { get; set; }

            protected virtual async System.Threading.Tasks.Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(System.Net.Http.HttpResponseMessage response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.Threading.CancellationToken cancellationToken)
            {
                if (response == null || response.Content == null)
                {
                    return new ObjectResponseResult<T>(default(T), string.Empty);
                }

                if (ReadResponseAsString)
                {
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    try
                    {
                        var typedBody = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
                        return new ObjectResponseResult<T>(typedBody, responseText);
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
                    }
                }
                else
                {
                    try
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        using (var streamReader = new System.IO.StreamReader(responseStream))
                        using (var jsonTextReader = new Newtonsoft.Json.JsonTextReader(streamReader))
                        {
                            var serializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings);
                            var typedBody = serializer.Deserialize<T>(jsonTextReader);
                            return new ObjectResponseResult<T>(typedBody, string.Empty);
                        }
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
                    }
                }
            }

            private string ConvertToString(object value, System.Globalization.CultureInfo cultureInfo)
            {
                if (value == null)
                {
                    return "";
                }

                if (value is System.Enum)
                {
                    var name = System.Enum.GetName(value.GetType(), value);
                    if (name != null)
                    {
                        var field = System.Reflection.IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
                        if (field != null)
                        {
                            var attribute = System.Reflection.CustomAttributeExtensions.GetCustomAttribute(field, typeof(System.Runtime.Serialization.EnumMemberAttribute))
                                as System.Runtime.Serialization.EnumMemberAttribute;
                            if (attribute != null)
                            {
                                return attribute.Value != null ? attribute.Value : name;
                            }
                        }

                        var converted = System.Convert.ToString(System.Convert.ChangeType(value, System.Enum.GetUnderlyingType(value.GetType()), cultureInfo));
                        return converted == null ? string.Empty : converted;
                    }
                }
                else if (value is bool)
                {
                    return System.Convert.ToString((bool)value, cultureInfo).ToLowerInvariant();
                }
                else if (value is byte[])
                {
                    return System.Convert.ToBase64String((byte[])value);
                }
                else if (value.GetType().IsArray)
                {
                    var array = System.Linq.Enumerable.OfType<object>((System.Array)value);
                    return string.Join(",", System.Linq.Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
                }

                var result = System.Convert.ToString(value, cultureInfo);
                return result == null ? "" : result;
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class PersonClient
        {
            private string _baseUrl = "https://localhost:7295";
            private System.Net.Http.HttpClient _httpClient;
            private System.Lazy<Newtonsoft.Json.JsonSerializerSettings> _settings;

            public PersonClient(System.Net.Http.HttpClient httpClient)
            {
                _httpClient = httpClient;
                _settings = new System.Lazy<Newtonsoft.Json.JsonSerializerSettings>(CreateSerializerSettings);
            }

            private Newtonsoft.Json.JsonSerializerSettings CreateSerializerSettings()
            {
                var settings = new Newtonsoft.Json.JsonSerializerSettings();
                UpdateJsonSerializerSettings(settings);
                return settings;
            }

            public string BaseUrl
            {
                get { return _baseUrl; }
                set { _baseUrl = value; }
            }

            protected Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

            partial void UpdateJsonSerializerSettings(Newtonsoft.Json.JsonSerializerSettings settings);

            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, string url);
            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, System.Text.StringBuilder urlBuilder);
            partial void ProcessResponse(System.Net.Http.HttpClient client, System.Net.Http.HttpResponseMessage response);

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> GetAsync()
            {
                return GetAsync(System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> GetAsync(System.Threading.CancellationToken cancellationToken)
            {
                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/persons");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PostAsync(string firstName, string middleName, string lastName, string cnic, System.DateTimeOffset? dateOfBirth, int? placeOfBirth, int? gender, int? familyUnit, int? fatherID, int? motherID)
            {
                return PostAsync(firstName, middleName, lastName, cnic, dateOfBirth, placeOfBirth, gender, familyUnit, fatherID, motherID, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PostAsync(string firstName, string middleName, string lastName, string cnic, System.DateTimeOffset? dateOfBirth, int? placeOfBirth, int? gender, int? familyUnit, int? fatherID, int? motherID, System.Threading.CancellationToken cancellationToken)
            {
                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/persons");

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var boundary_ = System.Guid.NewGuid().ToString();
                        var content_ = new System.Net.Http.MultipartFormDataContent(boundary_);
                        content_.Headers.Remove("Content-Type");
                        content_.Headers.TryAddWithoutValidation("Content-Type", "multipart/form-data; boundary=" + boundary_);

                        if (firstName != null)
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(firstName, System.Globalization.CultureInfo.InvariantCulture)), "FirstName");
                        }

                        if (middleName != null)
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(middleName, System.Globalization.CultureInfo.InvariantCulture)), "MiddleName");
                        }

                        if (lastName != null)
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(lastName, System.Globalization.CultureInfo.InvariantCulture)), "LastName");
                        }

                        if (cnic != null)
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(cnic, System.Globalization.CultureInfo.InvariantCulture)), "Cnic");
                        }

                        if (dateOfBirth == null)
                            throw new System.ArgumentNullException("dateOfBirth");
                        else
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(dateOfBirth, System.Globalization.CultureInfo.InvariantCulture)), "DateOfBirth");
                        }

                        if (placeOfBirth == null)
                            throw new System.ArgumentNullException("placeOfBirth");
                        else
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(placeOfBirth, System.Globalization.CultureInfo.InvariantCulture)), "PlaceOfBirth");
                        }

                        if (gender == null)
                            throw new System.ArgumentNullException("gender");
                        else
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(gender, System.Globalization.CultureInfo.InvariantCulture)), "Gender");
                        }

                        if (familyUnit == null)
                            throw new System.ArgumentNullException("familyUnit");
                        else
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(familyUnit, System.Globalization.CultureInfo.InvariantCulture)), "FamilyUnit");
                        }

                        if (fatherID == null)
                            throw new System.ArgumentNullException("fatherID");
                        else
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(fatherID, System.Globalization.CultureInfo.InvariantCulture)), "FatherID");
                        }

                        if (motherID == null)
                            throw new System.ArgumentNullException("motherID");
                        else
                        {
                            content_.Add(new System.Net.Http.StringContent(ConvertToString(motherID, System.Globalization.CultureInfo.InvariantCulture)), "MotherID");
                        }
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("POST");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> Get2Async(int id)
            {
                return Get2Async(id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> Get2Async(int id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/persons/{id}");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> PutAsync(int? personId, PersonWrapper wrapper, string id)
            {
                return PutAsync(personId, wrapper, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> PutAsync(int? personId, PersonWrapper wrapper, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                if (wrapper == null)
                    throw new System.ArgumentNullException("wrapper");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/persons/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (personId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("personId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(personId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        var json_ = Newtonsoft.Json.JsonConvert.SerializeObject(wrapper, _settings.Value);
                        var content_ = new System.Net.Http.StringContent(json_);
                        content_.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");
                        request_.Content = content_;
                        request_.Method = new System.Net.Http.HttpMethod("PUT");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<FileResponse> DeleteAsync(int? personId, string id)
            {
                return DeleteAsync(personId, id, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<FileResponse> DeleteAsync(int? personId, string id, System.Threading.CancellationToken cancellationToken)
            {
                if (id == null)
                    throw new System.ArgumentNullException("id");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/persons/{id}?");
                urlBuilder_.Replace("{id}", System.Uri.EscapeDataString(ConvertToString(id, System.Globalization.CultureInfo.InvariantCulture)));
                if (personId != null)
                {
                    urlBuilder_.Append(System.Uri.EscapeDataString("personId") + "=").Append(System.Uri.EscapeDataString(ConvertToString(personId, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
                }
                urlBuilder_.Length--;

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("DELETE");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/octet-stream"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200 || status_ == 206)
                            {
                                var responseStream_ = response_.Content == null ? System.IO.Stream.Null : await response_.Content.ReadAsStreamAsync().ConfigureAwait(false);
                                var fileResponse_ = new FileResponse(status_, headers_, responseStream_, null, response_);
                                disposeClient_ = false; disposeResponse_ = false; // response and client are disposed by FileResponse
                                return fileResponse_;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            protected struct ObjectResponseResult<T>
            {
                public ObjectResponseResult(T responseObject, string responseText)
                {
                    this.Object = responseObject;
                    this.Text = responseText;
                }

                public T Object { get; }

                public string Text { get; }
            }

            public bool ReadResponseAsString { get; set; }

            protected virtual async System.Threading.Tasks.Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(System.Net.Http.HttpResponseMessage response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.Threading.CancellationToken cancellationToken)
            {
                if (response == null || response.Content == null)
                {
                    return new ObjectResponseResult<T>(default(T), string.Empty);
                }

                if (ReadResponseAsString)
                {
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    try
                    {
                        var typedBody = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
                        return new ObjectResponseResult<T>(typedBody, responseText);
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
                    }
                }
                else
                {
                    try
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        using (var streamReader = new System.IO.StreamReader(responseStream))
                        using (var jsonTextReader = new Newtonsoft.Json.JsonTextReader(streamReader))
                        {
                            var serializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings);
                            var typedBody = serializer.Deserialize<T>(jsonTextReader);
                            return new ObjectResponseResult<T>(typedBody, string.Empty);
                        }
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
                    }
                }
            }

            private string ConvertToString(object value, System.Globalization.CultureInfo cultureInfo)
            {
                if (value == null)
                {
                    return "";
                }

                if (value is System.Enum)
                {
                    var name = System.Enum.GetName(value.GetType(), value);
                    if (name != null)
                    {
                        var field = System.Reflection.IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
                        if (field != null)
                        {
                            var attribute = System.Reflection.CustomAttributeExtensions.GetCustomAttribute(field, typeof(System.Runtime.Serialization.EnumMemberAttribute))
                                as System.Runtime.Serialization.EnumMemberAttribute;
                            if (attribute != null)
                            {
                                return attribute.Value != null ? attribute.Value : name;
                            }
                        }

                        var converted = System.Convert.ToString(System.Convert.ChangeType(value, System.Enum.GetUnderlyingType(value.GetType()), cultureInfo));
                        return converted == null ? string.Empty : converted;
                    }
                }
                else if (value is bool)
                {
                    return System.Convert.ToString((bool)value, cultureInfo).ToLowerInvariant();
                }
                else if (value is byte[])
                {
                    return System.Convert.ToBase64String((byte[])value);
                }
                else if (value.GetType().IsArray)
                {
                    var array = System.Linq.Enumerable.OfType<object>((System.Array)value);
                    return string.Join(",", System.Linq.Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
                }

                var result = System.Convert.ToString(value, cultureInfo);
                return result == null ? "" : result;
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class ReportsClient
        {
            private string _baseUrl = "https://localhost:7295";
            private System.Net.Http.HttpClient _httpClient;
            private System.Lazy<Newtonsoft.Json.JsonSerializerSettings> _settings;

            public ReportsClient(System.Net.Http.HttpClient httpClient)
            {
                _httpClient = httpClient;
                _settings = new System.Lazy<Newtonsoft.Json.JsonSerializerSettings>(CreateSerializerSettings);
            }

            private Newtonsoft.Json.JsonSerializerSettings CreateSerializerSettings()
            {
                var settings = new Newtonsoft.Json.JsonSerializerSettings();
                UpdateJsonSerializerSettings(settings);
                return settings;
            }

            public string BaseUrl
            {
                get { return _baseUrl; }
                set { _baseUrl = value; }
            }

            protected Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

            partial void UpdateJsonSerializerSettings(Newtonsoft.Json.JsonSerializerSettings settings);

            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, string url);
            partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, System.Text.StringBuilder urlBuilder);
            partial void ProcessResponse(System.Net.Http.HttpClient client, System.Net.Http.HttpResponseMessage response);

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<System.Collections.Generic.ICollection<string>> GetSiblingsAsync(int personid)
            {
                return GetSiblingsAsync(personid, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<System.Collections.Generic.ICollection<string>> GetSiblingsAsync(int personid, System.Threading.CancellationToken cancellationToken)
            {
                if (personid == null)
                    throw new System.ArgumentNullException("personid");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/reports/{personId}/siblings");
                urlBuilder_.Replace("{personid}", System.Uri.EscapeDataString(ConvertToString(personid, System.Globalization.CultureInfo.InvariantCulture)));

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/json"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200)
                            {
                                var objectResponse_ = await ReadObjectResponseAsync<System.Collections.Generic.ICollection<string>>(response_, headers_, cancellationToken).ConfigureAwait(false);
                                if (objectResponse_.Object == null)
                                {
                                    throw new ApiException("Response was null which was not expected.", status_, objectResponse_.Text, headers_, null);
                                }
                                return objectResponse_.Object;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual System.Threading.Tasks.Task<System.Collections.Generic.ICollection<string>> GetParentsAsync(int personid)
            {
                return GetParentsAsync(personid, System.Threading.CancellationToken.None);
            }

            /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
            /// <exception cref="ApiException">A server side error occurred.</exception>
            public virtual async System.Threading.Tasks.Task<System.Collections.Generic.ICollection<string>> GetParentsAsync(int personid, System.Threading.CancellationToken cancellationToken)
            {
                if (personid == null)
                    throw new System.ArgumentNullException("personid");

                var urlBuilder_ = new System.Text.StringBuilder();
                urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/api/reports/{personId}/parents");
                urlBuilder_.Replace("{personid}", System.Uri.EscapeDataString(ConvertToString(personid, System.Globalization.CultureInfo.InvariantCulture)));

                var client_ = _httpClient;
                var disposeClient_ = false;
                try
                {
                    using (var request_ = new System.Net.Http.HttpRequestMessage())
                    {
                        request_.Method = new System.Net.Http.HttpMethod("GET");
                        request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/json"));

                        PrepareRequest(client_, request_, urlBuilder_);

                        var url_ = urlBuilder_.ToString();
                        request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                        PrepareRequest(client_, request_, url_);

                        var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                        var disposeResponse_ = true;
                        try
                        {
                            var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                            if (response_.Content != null && response_.Content.Headers != null)
                            {
                                foreach (var item_ in response_.Content.Headers)
                                    headers_[item_.Key] = item_.Value;
                            }

                            ProcessResponse(client_, response_);

                            var status_ = (int)response_.StatusCode;
                            if (status_ == 200)
                            {
                                var objectResponse_ = await ReadObjectResponseAsync<System.Collections.Generic.ICollection<string>>(response_, headers_, cancellationToken).ConfigureAwait(false);
                                if (objectResponse_.Object == null)
                                {
                                    throw new ApiException("Response was null which was not expected.", status_, objectResponse_.Text, headers_, null);
                                }
                                return objectResponse_.Object;
                            }
                            else
                            {
                                var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                                throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                            }
                        }
                        finally
                        {
                            if (disposeResponse_)
                                response_.Dispose();
                        }
                    }
                }
                finally
                {
                    if (disposeClient_)
                        client_.Dispose();
                }
            }

            protected struct ObjectResponseResult<T>
            {
                public ObjectResponseResult(T responseObject, string responseText)
                {
                    this.Object = responseObject;
                    this.Text = responseText;
                }

                public T Object { get; }

                public string Text { get; }
            }

            public bool ReadResponseAsString { get; set; }

            protected virtual async System.Threading.Tasks.Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(System.Net.Http.HttpResponseMessage response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.Threading.CancellationToken cancellationToken)
            {
                if (response == null || response.Content == null)
                {
                    return new ObjectResponseResult<T>(default(T), string.Empty);
                }

                if (ReadResponseAsString)
                {
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    try
                    {
                        var typedBody = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
                        return new ObjectResponseResult<T>(typedBody, responseText);
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
                    }
                }
                else
                {
                    try
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        using (var streamReader = new System.IO.StreamReader(responseStream))
                        using (var jsonTextReader = new Newtonsoft.Json.JsonTextReader(streamReader))
                        {
                            var serializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings);
                            var typedBody = serializer.Deserialize<T>(jsonTextReader);
                            return new ObjectResponseResult<T>(typedBody, string.Empty);
                        }
                    }
                    catch (Newtonsoft.Json.JsonException exception)
                    {
                        var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
                        throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
                    }
                }
            }

            private string ConvertToString(object value, System.Globalization.CultureInfo cultureInfo)
            {
                if (value == null)
                {
                    return "";
                }

                if (value is System.Enum)
                {
                    var name = System.Enum.GetName(value.GetType(), value);
                    if (name != null)
                    {
                        var field = System.Reflection.IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
                        if (field != null)
                        {
                            var attribute = System.Reflection.CustomAttributeExtensions.GetCustomAttribute(field, typeof(System.Runtime.Serialization.EnumMemberAttribute))
                                as System.Runtime.Serialization.EnumMemberAttribute;
                            if (attribute != null)
                            {
                                return attribute.Value != null ? attribute.Value : name;
                            }
                        }

                        var converted = System.Convert.ToString(System.Convert.ChangeType(value, System.Enum.GetUnderlyingType(value.GetType()), cultureInfo));
                        return converted == null ? string.Empty : converted;
                    }
                }
                else if (value is bool)
                {
                    return System.Convert.ToString((bool)value, cultureInfo).ToLowerInvariant();
                }
                else if (value is byte[])
                {
                    return System.Convert.ToBase64String((byte[])value);
                }
                else if (value.GetType().IsArray)
                {
                    var array = System.Linq.Enumerable.OfType<object>((System.Array)value);
                    return string.Join(",", System.Linq.Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
                }

                var result = System.Convert.ToString(value, cultureInfo);
                return result == null ? "" : result;
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("NJsonSchema", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class AdoptionWrapper
        {
            [Newtonsoft.Json.JsonProperty("adopterID", Required = Newtonsoft.Json.Required.Always)]
            public int AdopterID { get; set; }

            [Newtonsoft.Json.JsonProperty("childID", Required = Newtonsoft.Json.Required.Always)]
            public int ChildID { get; set; }

            [Newtonsoft.Json.JsonProperty("dateOfAdoption", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public System.DateTimeOffset DateOfAdoption { get; set; }

        }

        [System.CodeDom.Compiler.GeneratedCode("NJsonSchema", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class DeathWrapper
        {
            [Newtonsoft.Json.JsonProperty("personID", Required = Newtonsoft.Json.Required.Always)]
            public int PersonID { get; set; }

            [Newtonsoft.Json.JsonProperty("dateOfDeath", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public System.DateTimeOffset DateOfDeath { get; set; }

            [Newtonsoft.Json.JsonProperty("reason", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string Reason { get; set; }

        }

        [System.CodeDom.Compiler.GeneratedCode("NJsonSchema", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class DivorceWrapper
        {
            [Newtonsoft.Json.JsonProperty("husbandID", Required = Newtonsoft.Json.Required.Always)]
            public int HusbandID { get; set; }

            [Newtonsoft.Json.JsonProperty("wifeID", Required = Newtonsoft.Json.Required.Always)]
            public int WifeID { get; set; }

            [Newtonsoft.Json.JsonProperty("dateOfDivorce", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public System.DateTimeOffset DateOfDivorce { get; set; }

            [Newtonsoft.Json.JsonProperty("dateOfSeparation", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public System.DateTimeOffset DateOfSeparation { get; set; }

            [Newtonsoft.Json.JsonProperty("reasonOfSeparation", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string ReasonOfSeparation { get; set; }

        }

        [System.CodeDom.Compiler.GeneratedCode("NJsonSchema", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class FamilyUnitWrapper
        {
            [Newtonsoft.Json.JsonProperty("familyName", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string FamilyName { get; set; }

        }

        [System.CodeDom.Compiler.GeneratedCode("NJsonSchema", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class MarriageWrapper
        {
            [Newtonsoft.Json.JsonProperty("husbandID", Required = Newtonsoft.Json.Required.Always)]
            public int HusbandID { get; set; }

            [Newtonsoft.Json.JsonProperty("wifeID", Required = Newtonsoft.Json.Required.Always)]
            public int WifeID { get; set; }

            [Newtonsoft.Json.JsonProperty("dateOfMarriage", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public System.DateTimeOffset DateOfMarriage { get; set; }

        }

        [System.CodeDom.Compiler.GeneratedCode("NJsonSchema", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class PersonWrapper
        {
            [Newtonsoft.Json.JsonProperty("firstName", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string FirstName { get; set; }

            [Newtonsoft.Json.JsonProperty("middleName", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string MiddleName { get; set; }

            [Newtonsoft.Json.JsonProperty("lastName", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string LastName { get; set; }

            [Newtonsoft.Json.JsonProperty("cnic", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string Cnic { get; set; }

            [Newtonsoft.Json.JsonProperty("dateOfBirth", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public System.DateTimeOffset DateOfBirth { get; set; }

            [Newtonsoft.Json.JsonProperty("placeOfBirth", Required = Newtonsoft.Json.Required.Always)]
            public int PlaceOfBirth { get; set; }

            [Newtonsoft.Json.JsonProperty("gender", Required = Newtonsoft.Json.Required.Always)]
            public int Gender { get; set; }

            [Newtonsoft.Json.JsonProperty("familyUnit", Required = Newtonsoft.Json.Required.Always)]
            public int FamilyUnit { get; set; }

            [Newtonsoft.Json.JsonProperty("fatherID", Required = Newtonsoft.Json.Required.Always)]
            public int FatherID { get; set; }

            [Newtonsoft.Json.JsonProperty("motherID", Required = Newtonsoft.Json.Required.Always)]
            public int MotherID { get; set; }

        }

        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class FileResponse : System.IDisposable
        {
            private System.IDisposable _client;
            private System.IDisposable _response;

            public int StatusCode { get; private set; }

            public System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> Headers { get; private set; }

            public System.IO.Stream Stream { get; private set; }

            public bool IsPartial
            {
                get { return StatusCode == 206; }
            }

            public FileResponse(int statusCode, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.IO.Stream stream, System.IDisposable client, System.IDisposable response)
            {
                StatusCode = statusCode;
                Headers = headers;
                Stream = stream;
                _client = client;
                _response = response;
            }

            public void Dispose()
            {
                Stream.Dispose();
                if (_response != null)
                    _response.Dispose();
                if (_client != null)
                    _client.Dispose();
            }
        }


        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class ApiException : System.Exception
        {
            public int StatusCode { get; private set; }

            public string Response { get; private set; }

            public System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> Headers { get; private set; }

            public ApiException(string message, int statusCode, string response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, System.Exception innerException)
                : base(message + "\n\nStatus: " + statusCode + "\nResponse: \n" + ((response == null) ? "(null)" : response.Substring(0, response.Length >= 512 ? 512 : response.Length)), innerException)
            {
                StatusCode = statusCode;
                Response = response;
                Headers = headers;
            }

            public override string ToString()
            {
                return string.Format("HTTP Response: \n\n{0}\n\n{1}", Response, base.ToString());
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("NSwag", "13.18.2.0 (NJsonSchema v10.8.0.0 (Newtonsoft.Json v13.0.0.0))")]
        public partial class ApiException<TResult> : ApiException
        {
            public TResult Result { get; private set; }

            public ApiException(string message, int statusCode, string response, System.Collections.Generic.IReadOnlyDictionary<string, System.Collections.Generic.IEnumerable<string>> headers, TResult result, System.Exception innerException)
                : base(message, statusCode, response, headers, innerException)
            {
                Result = result;
            }
        }

    }
}
